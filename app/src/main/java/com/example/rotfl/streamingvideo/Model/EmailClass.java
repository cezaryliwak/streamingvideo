package com.example.rotfl.streamingvideo.Model;

import javax.mail.Address;

/**
 * Created by rotfl on 28.05.2016.
 */
public class EmailClass {

    String subject;
    Address[] from;
    String sentDate;

    public EmailClass() {
    }

    public EmailClass(Address[] from, String sentDate, String subject) {
        this.from = from;
        this.sentDate = sentDate;
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public Address[] getFrom() {
        return from;
    }

    public void setFrom(Address[] from) {
        this.from = from;
    }
}
