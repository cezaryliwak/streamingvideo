package com.example.rotfl.streamingvideo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rotfl.streamingvideo.FtpGallery.LazyAdapter;
import com.example.rotfl.streamingvideo.Model.FileName;
import com.example.rotfl.streamingvideo.Model.User;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class GalleryActivity extends AppCompatActivity {

    ListView list;
    Button LRButton;
    TextView info;
    EditText login, password;
    CheckBox checkBox;
    Button bbb;

    LazyAdapter adapter;
    Activity activity = this;
    AsyncHttpClient client;
    SQLiteDatabase userInfo;
    Cursor coursor;
    Context c = this;

    String url = "http://192.168.43.114/cameraPHP/";
    String url1 = "http://192.168.43.114/cameraPHP/";
    ArrayList<User> users = new ArrayList<>();
    ArrayList<FileName> nameFiles = new ArrayList<>();
    String[] adresIMG ;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        LRButton = (Button) findViewById(R.id.LRButton);
        bbb = (Button) findViewById(R.id.bbb);
        info = (TextView) findViewById(R.id.info);
        login = (EditText) findViewById(R.id.loginO);
        password = (EditText) findViewById(R.id.passwordO);
        list=(ListView)findViewById(R.id.list);
        checkBox = (CheckBox) findViewById(R.id.checkBox);



     //   Button b=(Button)findViewById(R.id.button1);

        createTableOrDatabase();
        sprawdz();
//        bbb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
        if(users.size()!=0){
            getFileName(url + "checkFile.php?nazwaKonta="+users.get(0).getLogin());
            wypisz();
            if (nameFiles.size() != 0) {
                for (FileName f : nameFiles) {
                }
            } else {
            }
        }

//            }
//        });
        if(adresIMG != null){
            System.out.println("eeeeeeeerrrrrrrrrr  blad");
            adapter=new LazyAdapter(this, adresIMG);
            list.setAdapter(adapter);
        }

    }

    public void getFileName(final String url){

        AsyncHttpClient client2 = new AsyncHttpClient();

        client2.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream inputStream = new ByteArrayInputStream(responseBody);

                try {
                    inputStream.read();
                    inputStream.close();

                    String text = new String(responseBody);

                    Gson gson = new Gson();
                    com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                    JsonArray jsonArray = jsonParser.parse(text).getAsJsonArray();

                    for (JsonElement jsonElement : jsonArray) {
                        FileName nameFile = gson.fromJson(jsonElement, FileName.class);
                        nameFiles.add(nameFile);
                    }

                    adresIMG = new String[nameFiles.size()];
                    System.out.println(users.get(0).getLogin() + " zzzzzzzzzzzzzzxxxxxxxxx");

                    if(nameFiles.size() ==1){
                        Toast t = Toast.makeText( c ,"Brak plików w folderze", Toast.LENGTH_LONG );
                        t.show();
                    }else{
                        for(int i=0;i<nameFiles.size();i++){
                            adresIMG[i] = url1 +users.get(0).getLogin()+"/"+ nameFiles.get(i).getNazwapliku();
                            System.out.println(" " + adresIMG[i]);
                        }
                        bbb.setOnClickListener(listener);
                        adapter=new LazyAdapter(activity, adresIMG);
                        list.setAdapter(adapter);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("aaaaaaaaassssssssssaaaaaaaasssssss " + adresIMG[position]);
                Intent i = new Intent(getApplicationContext(), ObrazActivity.class);
                i.putExtra("url", adresIMG[position]);
                startActivity(i);
            }
        });
    }

    public void sprawdz(){      //TODO zadaniem metody jest sprawdzenie czy istnieje jakiś zalogowany uzytkownik na telefoie,
                                //TODO jesli nie istenieje to zostaje on zajrejestrowany, a jesli istnieje ale nie jest zalogowany to
                                //TODO uzytkownik mooze sie zalogować i zapamietać swoje haslo
        int i=0;
        users.clear();
        coursor = userInfo.rawQuery("Select * from User", null);

        if (coursor.getCount()!=0){
            LRButton.setVisibility(View.INVISIBLE);
            login.setVisibility(View.INVISIBLE);
            password.setVisibility(View.INVISIBLE);
            checkBox.setVisibility(View.INVISIBLE);

            coursor.moveToFirst();
            do{
                //TODO pobranie danych z bazy danych i zapisanie ich do obiektu,
                //TODO a nastepnie dodanie wszystkiego do listy
                user = new User(
                        coursor.getString(0),
                        coursor.getString(1),
                        coursor.getString(2)
                );
                users.add(user);
                coursor.moveToNext();
                i++;
            }while(coursor.getCount()!=i);

        }else if(coursor.getCount() ==0){       //TODO wyswietlanie przy pierwszym uruchomienieu aplikacji
                                                //TODO przycisk do dodania nowej kamery
            bbb.setVisibility(View.INVISIBLE);
            list.setVisibility(View.INVISIBLE); //TODO ukrycie listy
                      //TODO jesli zaznaczone jest "posiadam konto" wtedy nastepuje logowanie
                                                //TODO poprzez pobranie loginu i hasla z basy MySql
                LRButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkBox.isChecked()) {
                            url += "login.php?" + "login=" + login.getText() + "&password=" + password.getText();
                            pobierzUzytkownika(url);
                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(i);
                        }else if(checkBox.isChecked() == false){
                            LRButton.setText("Rejestrowanie");
                                    url += "register.php?" + "login=" + login.getText() + "&password=" + password.getText();
                                    dodajUzytkownika(url, login.getText().toString(), password.getText().toString());
                                    Intent i = new Intent(getApplicationContext(), GalleryActivity.class);
                                    startActivity(i);
                                }
                        }
                });
               //TODO jesli nie jest zaznaczone

        }
    }

    public void pobierzUzytkownika(String url){
        AsyncHttpClient client2 = new AsyncHttpClient();

        client2.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream inputStream = new ByteArrayInputStream(responseBody);

                try {
                    inputStream.read();
                    inputStream.close();

                    String text = new String(responseBody);
                    System.out.println("wwwwwwwwwwww " + text);
                    Gson gson = new Gson();
                    com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                    JsonArray jsonArray = jsonParser.parse(text).getAsJsonArray();

                    for (JsonElement jsonElement : jsonArray) {
                        User user1 = gson.fromJson(jsonElement, User.class);
                        users.add(user1);
                    }
                    if(users.size() >0){
                        String a =
                                "INSERT INTO User('login','password') VALUES" +
                                        "("
                                        +"'"+ login.getText() + "'," +
                                        "'" + password.getText()+"'"+
                                        "); ";
                        userInfo.execSQL(a);

                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });


    }

    public void dodajUzytkownika(String url,  final String login, final String password){
        AsyncHttpClient client2 = new AsyncHttpClient();

        client2.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream inputStream = new ByteArrayInputStream(responseBody);

                try {
                    inputStream.read();
                    inputStream.close();

                    String a =
                            "INSERT INTO User('login','password') VALUES" +
                                    "("
                                    +"'"+ login + "'," +
                                    "'" + password +"'"+
                                    "); ";
                    userInfo.execSQL(a);

                } catch (IOException e) {
                    System.out.println("wystąpił błąd rejestracji");
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
        Toast info = Toast.makeText(c,"Zarejestrowano pomyślnie", Toast.LENGTH_LONG);

    }

    public void createTableOrDatabase(){
        userInfo = openOrCreateDatabase("cameraInfo",MODE_PRIVATE, null);//stworzenie lokalnej bazy danych
        userInfo.execSQL("CREATE TABLE IF NOT EXISTS User(" +
                "id integer not null primary key AUTOINCREMENT," +
                "login varchar(30) not null,"+
                "password varchar(30) not null"+
                ");");
    }
    @Override
    public void onDestroy()
    {
        list.setAdapter(null);
        super.onDestroy();
    }

    public View.OnClickListener listener=new View.OnClickListener(){
        @Override
        public void onClick(View arg0) {
            adapter.imageLoader.clearCache();
            adapter.notifyDataSetChanged();
        }
    };

public void wypisz(){
    for(int i=0;i<nameFiles.size();i++){
            adresIMG[i] = nameFiles.get(i).getNazwapliku();
        System.out.println(nameFiles.get(i).getNazwapliku() + " aaaaaaaaabbbbbbbvvvvvvvvvvvv");
    }
}
}
