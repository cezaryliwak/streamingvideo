package com.example.rotfl.streamingvideo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.rotfl.streamingvideo.FtpGallery.ImageLoader;

/**
 * Created by rotfl on 07.09.2016.
 * Klasa ma za zadanie wywietlenie powiększonego zdjęcia pobranego z galerii
 */
public class ObrazActivity extends Activity{

    ImageView image;

    ImageLoader im = new ImageLoader(this);
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_layout);

        image = (ImageView) findViewById(R.id.imageView2);
        Intent i = getIntent();
        Bundle pobierzDane = i.getExtras();
        url = pobierzDane.getString("url");
        im.DisplayImage(url, image);
    }
}
