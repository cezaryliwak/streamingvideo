package com.example.rotfl.streamingvideo;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.NotificationCompat;

import com.example.rotfl.streamingvideo.Model.FileName;
import com.example.rotfl.streamingvideo.Model.User;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.apache.http.Header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by rotfl on 10.07.2016.
 */
public class RSSPullService extends Service {

    int iloscPlikow = 0;
    String url = "http://192.168.43.114/cameraPHP/checkFile.php?nazwaKonta=";

    User user;
    Cursor userCursor;
    ArrayList<User> users = new ArrayList<>();
    Context context = this;
    int i=0;

    MainActivity mainActivity = new MainActivity();
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
     //   Toast.makeText(this, "Service started", Toast.LENGTH_LONG).show();
        System.out.println("wszedlem do rssPullService");
        final SQLiteDatabase cameraInfo  = openOrCreateDatabase("cameraInfo", MODE_PRIVATE, null);//stworzenie lub otworzenie lokalnej bazy danych
                        userCursor = cameraInfo.rawQuery("Select * from User", null);
        int staraIlosc = userCursor.getCount();

        users.clear();
        userCursor = cameraInfo.rawQuery("Select * from User", null);
        userCursor.moveToFirst();
        do{
            //TODO pobranie danych z bazy danych i zapisanie ich do obiektu,
            //TODO a nastepnie dodanie wszystkiego do listy
            user = new User(
                    userCursor.getString(0),
                    userCursor.getString(1),
                    userCursor.getString(2)
            );
            users.add(user);
            userCursor.moveToNext();
            i++;
        }while(userCursor.getCount()!=i);

        final int[] iloscPlikowTemp = {getFile(users.get(0).getLogin())};

        Thread t =  new Thread(new Runnable() {
            @Override
            public void run() {
                int i=0;
                do{
                    try {
                        userCursor = cameraInfo.rawQuery("Select * from User", null);
                        if(getFile(users.get(0).getLogin())> iloscPlikowTemp[0]){
                            iloscPlikowTemp[0] = getFile(users.get(0).getLogin());
                            createNotification();
                            System.out.println("aaaaaaaaassssssssss " +getFile(users.get(0).getLogin()));
                        }
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }while (userCursor.getCount()>0);
            }
        });
        t.start();

        onDestroy();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public int getFile(String user){
        final int[] ilosc = new int[1];
        SyncHttpClient client2 = new SyncHttpClient();

        client2.get(url + user, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream inputStream = new ByteArrayInputStream(responseBody);

                try {
                    inputStream.read();
                    inputStream.close();

                    String text = new String(responseBody);

                    Gson gson = new Gson();
                    com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                    JsonArray jsonArray = jsonParser.parse(text).getAsJsonArray();

                    for (JsonElement jsonElement : jsonArray) {
                        FileName nameFile = gson.fromJson(jsonElement, FileName.class);
                    }
                    ilosc[0] = jsonArray.size();
                    System.out.println("ppppppppppppppppp " + ilosc[0]);


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });

        return ilosc[0];
    }

    public void createNotification(){
        Intent notificationIntent = new Intent(context, GalleryActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(context, 0,
                notificationIntent, 0);

        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.info)
                        .setContentTitle("Ostrzeżenie!")
                        .setContentText("Twoja kamera wykryła ruch!")
                .setContentIntent(intent);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);


        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(1, mBuilder.build());
    }
}
