package com.example.rotfl.streamingvideo.Model;

/**
 * Created by rotfl on 04.06.2016.
 */
public class User {


    public User(String id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
    }

    /**
     * id : 1
     * login : cezary
     * password : liwak
     */

    private String id;
    private String login;
    private String password;

    public void setId(String id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
