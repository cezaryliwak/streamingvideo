package com.example.rotfl.streamingvideo.Model;

/**
 * Created by rotfl on 24.06.2016.
 */
public class FileName {

    String nazwaPliku;
    /**
     * nazwapliku : 63.jpg
     */

    private String nazwapliku;

    public FileName(String nazwaPliku) {
        this.nazwaPliku = nazwaPliku;
    }


    public void setNazwapliku(String nazwapliku) {
        this.nazwapliku = nazwapliku;
    }

    public String getNazwapliku() {
        return nazwapliku;
    }
}
