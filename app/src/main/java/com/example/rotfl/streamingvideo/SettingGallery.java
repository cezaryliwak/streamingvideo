package com.example.rotfl.streamingvideo;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rotfl.streamingvideo.Model.User;

import java.util.ArrayList;

public class SettingGallery extends AppCompatActivity {

    private TextView loginS, passwordS;
    private Button wylogujButton;

    private String url = "http://192.168.43.114/cameraPHP";

    SQLiteDatabase userInfo;
    Cursor coursor;
    User user;
    ArrayList<User> users = new ArrayList<>();
    int i=0;
    Context c = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_gallery);
        userInfo = openOrCreateDatabase("cameraInfo", MODE_PRIVATE, null);

        loginS = (TextView) findViewById(R.id.loginS);
        passwordS = (TextView) findViewById(R.id.passwordS);
        wylogujButton = (Button) findViewById(R.id.wylogujButton);


        coursor = userInfo.rawQuery("Select * from User", null);

        if (coursor.getCount() != 0) {      //TODO jesli tabela User nie jest pusta to automatycznie wypełniane są
                                            //TODO pola i uzytkownik ma możliwość przelogowania sie na inne konto

            coursor.moveToFirst();
            do {
                users.clear();
                //TODO pobranie danych z bazy danych i zapisanie ich do obiektu,
                //TODO a nastepnie dodanie wszystkiego do listy
                user = new User(
                        coursor.getString(0),
                        coursor.getString(1),
                        coursor.getString(2)
                );
                users.add(user);
                for(User u : users){
                    System.out.println("wwwwwww" + u.getLogin() + u.getPassword() + u.getId() );
                }

                coursor.moveToNext();
                i++;

            } while (coursor.getCount() != i);
        }
        if(users.size()!=0){
            loginS.setText(users.get(0).getLogin().trim());
            passwordS.setText((users.get(0).getPassword()).trim());
            wylogujButton.setText("Wyloguj");

                // Jeśli aplikacja wykryła zalogowanego użytkownika wyświetli ona przycisk z napisem WYLOGUJ
                // Po jego wciśnięciu użytkownik zostaje wylogowany
            wylogujButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    wyloguj();
                }
            });
        }else if(users.size() ==0){
            Intent i = new Intent(getApplicationContext(), GalleryActivity.class);
            startActivity(i);
        }

    }
    private void wyloguj(){
        String a =
                "DELETE FROM User" +
                        " ";
        userInfo.execSQL(a);
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);

        Toast info = Toast.makeText(c,"Wylogowano pomyślnie", Toast.LENGTH_LONG);
    }
}
