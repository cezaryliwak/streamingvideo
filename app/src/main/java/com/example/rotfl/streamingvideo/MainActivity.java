package com.example.rotfl.streamingvideo;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.rotfl.streamingvideo.Model.DataClass;
import com.example.rotfl.streamingvideo.Model.User;

import java.util.ArrayList;
import java.util.List;

/*
* Główny ekran po uruchomieniu aplikacji
* */
public class MainActivity extends AppCompatActivity {

    ListView cameraList;
    Button addCamera;
    TextView textDodajKamere;

    int i = 0;


    AlertDialog.Builder alertDialog;
    Context context = this;
    SQLiteDatabase cameraInfo;
    Cursor coursor;
    Cursor userCursor;
    ArrayList<DataClass> dataClasses = new ArrayList<>();
    ArrayList<String> dataClassesString = new ArrayList<>();

    ArrayList<User> userGallery = new ArrayList<>();


    ArrayList<String> menuArrayList = new ArrayList<>();    //lista z elementami opcji menu
    CharSequence[] cameraMenuList = new CharSequence[3];    // tablica z elementami menu


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cameraInfo = openOrCreateDatabase("cameraInfo", MODE_PRIVATE, null);//stworzenie lub
                                                                // otworzenie lokalnej bazy danych

        cameraList = (ListView) findViewById(R.id.cameraList);
        addCamera = (Button) findViewById(R.id.addCamera);
        textDodajKamere = (TextView) findViewById(R.id.textDodajKamere);

        alertDialog = new AlertDialog.Builder(context);
        

        // TODO******************* Dodanie opcji do menu kamer - wyswietlane po ***************************
        // TODO******************** długim przytrzymaniu odpowiedniej kamery ******************************
        menuArrayList.add("Zobacz obraz");
        menuArrayList.add("Galeria");
        menuArrayList.add("Edytuj");
        menuArrayList.add("Usuń");

        cameraMenuList = menuArrayList.toArray(new CharSequence[menuArrayList.size()]); //wpisanie elementów listy do CharSequence
        //TODO****************************************************************************************

        createTableOrDatabase();    // tworzy tabelę i lokalą bazę danych jeśli nie została wcześniej
                                    // stworzona
        getAllCamera();             //pobiera i zapisuje wszystkie kamery zapisane w lokalnej bazie
        chekGallery();

        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> tasks = activityManager. getRunningServices(Integer.MAX_VALUE);

        //TODO**************** Zmiana ArrayList na ArayAdapter na potrzeby ***********************
        //TODO********************* dodania elementów do ListView ********************************
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                dataClassesString
        );
        cameraList.setAdapter(arrayAdapter);    // wpisanie elementów do list view
        //TODO*************************************************************************************
        //TODO************* Pobranie pozycji po wybraniu elementu z listy i przejscie do video*****
        cameraList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                alertDialog.setTitle("Co chcesz zrobić ?");
                menuCamera(position);
            }
        });

        //TODO************************************************************************************
        //TODO ********* Event na dotknięcie i przytrzymanie przycisku kamery
        cameraList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                alertDialog.setTitle("Co chcesz zrobić ?");
                menuCamera(position);
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }



    //********************** Metoda tworzy bazę danych - jesli nie istnieje i tworzy tabele   *****
    //********************** a jeśli istnieje to otwiera bazę *************************************
    public void createTableOrDatabase() {
        cameraInfo = openOrCreateDatabase("cameraInfo", MODE_PRIVATE, null);//stworzenie lokalnej
                                                                            // bazy danych
//        cameraInfo.execSQL("DROP TABLE cameraSettings");
        cameraInfo.execSQL("CREATE TABLE IF NOT EXISTS cameraSettings(" +
                "id integer not null primary key AUTOINCREMENT , " +
                "nameCamera varchar(30) not null, " +
                "rtspAdress varchar(100) not null," +
                "login varchar(30) not null, " +
                "password varchar(30) not null, " +
                "port int not null," +
                "portHTTP int not null," +
                "brightness int not null, " +
                "contrast int not null, " +
                "saturation int not null, " +
                "hue int not null " +
                ");");
        cameraInfo.execSQL("CREATE TABLE IF NOT EXISTS User(" +
                "id integer not null primary key AUTOINCREMENT," +
                "login varchar(30) not null," +
                "password varchar(30) not null" +
                ");");
    }
    //*********************************************************************************************

    public void getAllCamera() {
        dataClasses.clear();
        dataClassesString.clear();
        coursor = cameraInfo.rawQuery("Select * from cameraSettings", null);

        if (coursor.getCount() != 0) {
            addCamera.setVisibility(View.INVISIBLE);
            textDodajKamere.setVisibility(View.INVISIBLE);
            coursor.moveToFirst();
            do {
                // pobranie danych z bazy danych i zapisanie ich do obiektu,
                // a nastepnie dodanie obiektu do listy
                DataClass dtC = new DataClass(
                        coursor.getString(0),
                        coursor.getString(1),//nazwa kamery
                        coursor.getString(2),//adres rtsp
                        coursor.getString(3),//login
                        coursor.getString(4),//hasło
                        coursor.getString(5),// port rtsp
                        coursor.getString(6),// port http
                        coursor.getString(7),//jasność
                        coursor.getString(8),//kontrast
                        coursor.getString(9),//nasycenie
                        coursor.getString(10)//barwa
                );

                // zapisanie informacji które mają zostać wyswietlone  w listView
                dataClassesString.add(coursor.getString(1) + "\n" + coursor.getString(2));

                dataClasses.add(dtC);
                coursor.moveToNext();
                i++;
            } while (coursor.getCount() != i);
        } else if (coursor.getCount() == 0) {       // wyswietlanie przy pierwszym
                                                    // uruchomienieu aplikacji
            // przycisk do dodania nowej kamery
            addCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), SettingActivity.class);
                    i.putExtra("opcja", "dodaj");
                    startActivity(i);
                }
            });
        }
    }

    //TODO********** Wyswietlenie menu po przytrzymaniu "kamery" ******************************
    public void menuCamera(final int position) {

        alertDialog.setItems(cameraMenuList, new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {     //TODO Jesli wybane zostało "Zobacz obraz"
                    Intent i = new Intent(getApplicationContext(), VideoActivity.class);
                    i.putExtra("idCamera", dataClasses.get(position).getIdCamera());
                    i.putExtra("rtspAdress", dataClasses.get(position).getRtspAdress());
                    i.putExtra("login", dataClasses.get(position).getLogin());
                    i.putExtra("password", dataClasses.get(position).getPassword());
                    i.putExtra("port", dataClasses.get(position).getPort());
                    startActivity(i);

                } else if (which == 1) {       //TODO Jeśli wybrane zostało "Galeria"
                    Intent i = new Intent(getApplicationContext(), GalleryActivity.class);
                    startActivity(i);
                } else if (which == 2) {   //TODO jeśli wybrane zostało "Edytuj"
                    try {
                        //Przejście do ConnectionSettings i przekazanie wartości
                        Intent i = new Intent(getApplicationContext(), SettingActivity.class);
                        i.putExtra("opcja", "edytuj");
                        i.putExtra("idCamera", dataClasses.get(position).getIdCamera());
                        i.putExtra("nameCamera", dataClasses.get(position).getNameCamera().toString());
                        i.putExtra("rstpAdress", dataClasses.get(position).getRtspAdress().toString());
                        i.putExtra("login", dataClasses.get(position).getLogin().toString());
                        i.putExtra("password", dataClasses.get(position).getPassword().toString());
                        i.putExtra("port", dataClasses.get(position).getPort().toString());
                        i.putExtra("portHTTP", dataClasses.get(position).getPortHTTP().toString());
                        i.putExtra("opcja", "edytuj");
                        startActivity(i);
                    } catch (Exception e) {
                        System.out.println(e);
                    }

                } else if (which == 3) {  //TODO jeśli jest wybrane "Usun"
                    deleteCamera(position);
                }
            }
        });
        alertDialog.show();

    }

    //TODO*************** Usuwanie kamery z listy ********************************************
    public void deleteCamera(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Czy chcesz usunąć kamerę z listy?")
                .setTitle("Usuń");

        builder.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                cameraInfo.execSQL(
                        "DELETE FROM cameraSettings " +
                                "WHERE id = " + dataClasses.get(position).getIdCamera()
                );
                cameraInfo.execSQL("DELETE FROM User");
                //************************* Odświeżanie aktywności *****************************
                finish();                   // zamknięcie aktywności
                startActivity(getIntent()); // ponowne uruchomienie aktywności
            }
        })
                .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialgo, int id) {
                        // TODO tu można wpisać, jakie działanie ma się wykonać po wciśnieciu przycisku nie
                    }
                });
        builder.show();
    }

    //metoda sprawdza czy na urządzeniu zalogowany jest jakiś użytkownik,
    // 8jeśli tak, zostanie uruchomiony
    //serwis sprawdzający wykrycie ruchu
    public void chekGallery(){
        userCursor = cameraInfo.rawQuery("Select * from User", null);

        if (userCursor.getCount() != 0) {
            try{
                stopService(new Intent(getBaseContext(), RSSPullService.class));
                startService(new Intent(getBaseContext(), RSSPullService.class));
            }catch (Exception e){
                System.out.println(e);
            }

        }else if(coursor.getCount() == 0){
            stopService(new Intent(getBaseContext(),RSSPullService.class));
        }
    }

}