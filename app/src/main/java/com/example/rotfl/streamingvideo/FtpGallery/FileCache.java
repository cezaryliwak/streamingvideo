package com.example.rotfl.streamingvideo.FtpGallery;


import android.content.Context;

import java.io.File;

/**
 * Created by rotfl on 03.06.2016.
 */
public class FileCache {

    private File cacheDir;

    public FileCache(Context context){
        //Znajdź dir aby zapisać obrazek w pamieci podrecznej
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"LazyList");
        else
            cacheDir=context.getCacheDir();
        if(!cacheDir.exists())
            cacheDir.mkdirs();
    }

    public File getFile(String url){
        //I identify images by hashcode. Not a perfect solution, good for the demo.
        String filename=String.valueOf(url.hashCode());
        File f = new File(cacheDir, filename);
        return f;
    }
    public void clear(){
        File[] files=cacheDir.listFiles();
        if(files==null)
            return;
        for(File f:files)
            f.delete();
    }

}